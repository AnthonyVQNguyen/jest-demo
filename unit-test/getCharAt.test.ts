import { getCharAt } from "../api-test/getCharAt";

//Contour Global
test("pos 0", () => {
  expect(getCharAt(0)).toBe("C");
});

test("pos 8", () => {
  expect(getCharAt(8)).toBe("G");
});
