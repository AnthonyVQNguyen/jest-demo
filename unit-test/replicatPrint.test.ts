import * as fn from "../api-test/callCharAtEndpoint";
import { replicaPrint } from "../api-test/replicaPrint";

const mockFn = jest.spyOn(fn, "callCharAtEndpoint").mockResolvedValue("J");

test("pos 0, 3 times", async () => {
  expect(await replicaPrint(0, 3)).toBe("JJJ");
  expect(mockFn).toHaveBeenCalledTimes(1);
});
