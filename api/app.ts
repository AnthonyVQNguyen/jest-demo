import express from "express";

const app = express();
const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send("Hello Contour");
});

app.get("/charAt", (req, res) => {
  const data = "Contour Network";
  const pos = req.query.pos;
  const char = data.charAt(Number.parseInt(pos as string));
  res.send(char);
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
