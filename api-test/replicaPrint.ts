import { callCharAtEndpoint } from "./callCharAtEndpoint";

export async function replicaPrint(pos:number, time:number){
    const char = await callCharAtEndpoint(pos);
    return char.repeat(time);
}