import chai from "chai";
import chaiHttp from "chai-http";
import { getCharAt } from "../getCharAt";

chai.use(chaiHttp);

test("test /charAt endpoint", async () => {
  const agent = chai.request("http://localhost:3000");
  
  for (var i = 0; i < 10; i++) {
    const res = await agent.get("/charAt").query({ pos: i });
    expect(res.status).toBe(200);
    expect(res.text).toBe(getCharAt(i));
  }
});
