const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);


test("test / endpoint", async () => {
    const agent = chai.request("http://localhost:3000");
    const res = await agent.get("/");

    expect(res.status).toBe(200);
    expect(res.text).toBe("Hello Contour");
})