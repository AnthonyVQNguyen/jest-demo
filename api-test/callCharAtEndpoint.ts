import chai from "chai";
import chaiHttp from "chai-http";

chai.use(chaiHttp);

export async function callCharAtEndpoint(pos: number) {
    const agent = chai.request("http://localhost:3000");
    const res = await agent.get("/charAt").query({ pos: pos });
    return res.text;
}